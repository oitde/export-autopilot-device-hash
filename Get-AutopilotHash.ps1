﻿#Run as Administrator

#Run this step first by clicking on line 4 and pressing F8. Then run the full script by pressing F5.
Set-ExecutionPolicy Unrestricted -Scope Process -Force

$EmailRecipient= ("oitde@duke.edu")
$Timestamp = Get-Date -Format "MM/dd/yyyy h:mm tt"
$SerialNumber = (Get-CimInstance -ClassName win32_bios).SerialNumber
$OSVersion = (Get-CimInstance Win32_OperatingSystem).Caption
$OSBuild = (Get-CimInstance Win32_OperatingSystem).Version

Register-PackageSource -Name MyNuGet -Location https://www.nuget.org/api/v2 -ProviderName NuGet -ForceBootstrap
Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted
Install-Script -Name Get-WindowsAutoPilotInfo -Force
Get-WindowsAutoPilotInfo.ps1 -OutputFile "C:\users\$ENV:USERNAME\desktop\$SerialNumber.csv"

$AutopilotHashCSV= Get-ChildItem -Path "C:\users\$ENV:USERNAME\desktop\$SerialNumber.csv" 
Send-MailMessage `
            -From "AutopilotHashUploader@duke.edu" `
            -To $EmailRecipient `
            -CC "ae57@duke.edu" `
            -Subject "New Autopilot Hash Available" `
            -Body "A new Autopilot hash is attached. Hash exported at $Timestamp by $ENV:USERNAME with serial number $SerialNumber for operating system $OSVersion $OSBuild" `
            -Attachments $AutopilotHashCSV `
            -SmtpServer 'smtp.duke.edu'

#Remove-Item $AutopilotHashCSV -Force